# This is C language lab repo
* More further information at:
  * https://gitlab.se.ifmo.ru/c-language/assignment-memory-allocator

# How to run 
* To compile code type: `make` 
* To run code type: `make run`
* To clean remove build directory type: `make clean`