#include <stdarg.h>
#define _DEFAULT_SOURCE
#include <unistd.h>
#include <stdio.h>
#include <stddef.h>
#include <stdlib.h>
#include <assert.h>
#include "error_io.h"

#include "mem_internals.h"
#include "mem.h"
#include "util.h"

void debug_block(struct block_header* b, const char* fmt, ... );
void debug(const char* fmt, ... );

extern inline block_size size_from_capacity( block_capacity cap );
extern inline block_capacity capacity_from_size( block_size sz );

static bool            block_is_big_enough( size_t query, struct block_header* block ) { return block->capacity.bytes >= query; }
static size_t          pages_count   ( size_t mem )                      { return mem / getpagesize() + ((mem % getpagesize()) > 0); }
static size_t          round_pages   ( size_t mem )                      { return getpagesize() * pages_count( mem ) ; }

static void block_init( void* restrict addr, block_size block_sz, void* restrict next ) {
  *((struct block_header*)addr) = (struct block_header) {
    .next = next,
    .capacity = capacity_from_size(block_sz),
    .is_free = true
  };
}

static size_t region_actual_size( size_t query ) { return size_max( round_pages( query ), REGION_MIN_SIZE ); }

extern inline bool region_is_invalid( const struct region* r );



static void* map_pages(void const* addr, size_t length, int additional_flags) {
  return mmap( (void*) addr, length, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS | additional_flags , 0, 0 );
}

/*  аллоцировать регион памяти и инициализировать его блоком */
static struct region alloc_region  ( void const * addr, size_t query ) {
  struct region allocated_region;
  query = region_actual_size(query);

  void* new_address = map_pages(addr, query, MAP_FIXED);
  if(new_address == MAP_FAILED){
    new_address = map_pages(addr, query, MAP_FILE); 
    allocated_region = (struct region){new_address, query, false};
  }else{
    allocated_region = (struct region){new_address, query, true};
  }
  block_init(new_address, (block_size){query}, NULL);
  return allocated_region;
}

static void* block_after( struct block_header const* block )         ;

void* heap_init( size_t initial ) {
  const struct region region = alloc_region( HEAP_START, initial );
  if ( region_is_invalid(&region) ) return NULL;

  return region.addr;
}

#define BLOCK_MIN_CAPACITY 24

/*  --- Разделение блоков (если найденный свободный блок слишком большой )--- */

static bool block_splittable( struct block_header* restrict block, size_t query) {
  return block-> is_free && query + offsetof( struct block_header, contents ) + BLOCK_MIN_CAPACITY <= block->capacity.bytes;
}

static bool split_if_too_big( struct block_header* block, size_t query ) {

  if(!block){
    RED("Invalid block!");
    return false;
  }

  if(block_splittable(block,query)){
    struct block_header * const cut_block = (struct block_header* const)(block->contents + query);
    block_size cut_block_size = (block_size){block->capacity.bytes - query};
    block_init(cut_block, cut_block_size, block->next);
    block->next = cut_block;
    block->capacity.bytes = query;
    return true;
  }
  return false;
}

 static size_t block_actual_capacity( size_t query ) { return size_max( query, BLOCK_MIN_CAPACITY ); }


/*  --- Слияние соседних свободных блоков --- */

static void* block_after( struct block_header const* block )              {
  return  (void*) (block->contents + block->capacity.bytes);
}
static bool blocks_continuous (
                               struct block_header const* fst,
                               struct block_header const* snd ) {
  return (void*)snd == block_after(fst);
}

static bool mergeable(struct block_header const* restrict fst, struct block_header const* restrict snd) {
  return fst->is_free && snd->is_free && blocks_continuous( fst, snd ) ;
}

static bool try_merge_with_next( struct block_header* block ) {
  
  if(!block->next){
    RED("Last block!");
    return false;
  }

  struct block_header* const next_block = block->next;
  if(mergeable(block, next_block)){
    block->next = next_block->next;
    block->capacity.bytes += next_block->capacity.bytes + offsetof(struct block_header, contents);
    return true;
  }
  return false; 
}


/*  --- ... ecли размера кучи хватает --- */

struct block_search_result {
  enum {BSR_FOUND_GOOD_BLOCK, BSR_REACHED_END_NOT_FOUND, BSR_CORRUPTED} type;
  struct block_header* block;
};


static struct block_search_result find_good_or_last  ( struct block_header* restrict block, size_t sz )    {
  if(!block){
    RED("Invalid block!");
    return  (struct block_search_result){BSR_REACHED_END_NOT_FOUND, block};
  }
  struct block_header* current_block = block;

  while(current_block->next){
    if(current_block->is_free && block_is_big_enough(sz, current_block)){
      return (struct block_search_result){BSR_FOUND_GOOD_BLOCK, current_block};
    }
    if(!try_merge_with_next(current_block)){
      current_block = current_block->next;
    }
  }
  if(current_block->is_free && block_is_big_enough(sz, current_block)){
      return (struct block_search_result){BSR_FOUND_GOOD_BLOCK, current_block};
    }
  return (struct block_search_result){BSR_REACHED_END_NOT_FOUND, current_block};
}

/*  Попробовать выделить память в куче начиная с блока `block` не пытаясь расширить кучу
 Можно переиспользовать как только кучу расширили. */
static struct block_search_result try_memalloc_existing ( size_t query, struct block_header* block ) {
  if(!block){
    RED("Invalid block!");
    return  (struct block_search_result){BSR_REACHED_END_NOT_FOUND, block};
  }
  struct block_search_result lucky_block = find_good_or_last(block, query);
  if(lucky_block.type == BSR_FOUND_GOOD_BLOCK){
    split_if_too_big(lucky_block.block, query);
  }
  return lucky_block;
}



static struct block_header* grow_heap( struct block_header* restrict last, size_t query ) {
  if(!last){
    RED("Invalid block!");
    return last;
  }
  void* new_adress = last->contents + last->capacity.bytes;
  const struct region allocated_region = alloc_region(new_adress, query);
  last->next = allocated_region.addr;
  try_merge_with_next(last);
  return allocated_region.addr;
}

/*  Реализует основную логику malloc и возвращает заголовок выделенного блока */
static struct block_header* memalloc( size_t query, struct block_header* heap_start) {
  query = block_actual_capacity(query);
  struct block_search_result found_block = try_memalloc_existing(query, heap_start);
  if(found_block.type == BSR_FOUND_GOOD_BLOCK){
    found_block.block->is_free = false;
    return found_block.block;
  }
  grow_heap(found_block.block, query);
  found_block = try_memalloc_existing(query, heap_start);
  found_block.block->is_free = false;
  return found_block.block;
}

void* _malloc( size_t query ) {
  struct block_header* const addr = memalloc( query, (struct block_header*) HEAP_START );
  if (addr) return addr->contents;
  else return NULL;
}

static struct block_header* block_get_header(void* contents) {
  return (struct block_header*) (((uint8_t*)contents)-offsetof(struct block_header, contents));
}

void _free( void* mem ) {
  if (!mem) return ;
  struct block_header* header = block_get_header( mem );
  header->is_free = true;
  try_merge_with_next(header);
}

