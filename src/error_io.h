
#ifndef ERROR_IO
#define ERROR_IO
#include<stdio.h>

#define ANSI_COLOR_RED     "\x1b[31m"
#define ANSI_COLOR_GREEN   "\x1b[32m"
#define ANSI_COLOR_MAGENTA "\x1b[35m"
#define ANSI_COLOR_BLUE    "\x1b[34m"
#define ANSI_COLOR_RESET   "\x1b[0m"

#define RESET(...){ printf(ANSI_COLOR_RESET __VA_ARGS__);}

#define RED(...){ printf(ANSI_COLOR_RED __VA_ARGS__); RESET("");}
#define BLUE(...){ printf(ANSI_COLOR_BLUE __VA_ARGS__); RESET("");}
#define GREEN(...){ printf(ANSI_COLOR_GREEN __VA_ARGS__); RESET("");}
#define MAGENTA(...){ printf(ANSI_COLOR_MAGENTA __VA_ARGS__); RESET("");}


#endif
